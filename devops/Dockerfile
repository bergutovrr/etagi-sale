FROM python:3.7

WORKDIR /app


ADD src/requirements.txt requirements.txt

RUN apt-get update -qq && \
	DEBIAN_FRONTEND=noninteractive apt-get install -qq \
		python3-tk \
		xvfb \
		curl


RUN set -ex \
    && buildDeps=' \
        gcc \
        libbz2-dev \
        libc6-dev \
        libgdbm-dev \
        liblzma-dev \
        libblas-dev \
        liblapack-dev \
        libatlas-base-dev \
        libopenblas-dev \
        gfortran \
        libncurses-dev \
        libreadline-dev \
        libsqlite3-dev \
        libssl-dev \
        libpcre3-dev \
        make \
        tcl-dev \
        tk-dev \
        wget \
        xz-utils \
        zlib1g-dev \
    ' \
    && deps=' \
        libexpat1 \
    ' \
    && apt-get update && apt-get install -y $buildDeps $deps --no-install-recommends  && rm -rf /var/lib/apt/lists/* \
    && pip install --no-cache-dir -r requirements.txt \
    && apt-get purge -y --auto-remove $buildDeps \
    && find /usr/local -depth \
    \( \
        \( -type d -a -name test -o -name tests \) \
        -o \
        \( -type f -a -name '*.pyc' -o -name '*.pyo' \) \
    \) -exec rm -rf '{}' +


COPY src/ .

RUN  python manage.py collectstatic --noinput

CMD  ['/bin/sh','-c','python manage.py migrate --noinput && python cherry.py']
