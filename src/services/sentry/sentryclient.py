from abc import ABCMeta, abstractmethod

import requests
from django.conf import settings

from services.sentry.exceptions import SentryClientError
from atlas_utils.common.url import join


class AbstractSentryClient:
    __metaclass__ = ABCMeta

    @abstractmethod
    def get_sentry_projects(self):
        raise NotImplementedError

    @abstractmethod
    def get_sentry_project(self, project_name: str):
        raise NotImplementedError

    @abstractmethod
    def get_sentry_project_keys(self, project_name: str):
        raise NotImplemented

    @abstractmethod
    def create_sentry_project(self, team_name: str, project_name: str):
        raise NotImplemented

    @abstractmethod
    def get_sentry_team(self, team_name: str):
        raise NotImplemented

    @abstractmethod
    def create_sentry_team(self, team_name: str):
        raise NotImplemented


class SentryClient(AbstractSentryClient):

    def __init__(self):
        self.url = settings.SENTRY_URL_FOR_EXAMPLE
        self.token = settings.SENTRY_API_TOKEN

    def _sender(self, endpoint, data=None, method='GET'):
        """
        Отправка запроса в Sentry
        """
        endpoint = join(self.url, f'/api/0{endpoint}')
        headers = {'Authorization': "Bearer " + self.token}
        req = requests.request(method=method, url=endpoint, data=data, headers=headers)
        if req.ok:
            return req.json()
        elif req.status_code == 404:
            return None
        else:
            raise SentryClientError(req)

    def get_sentry_projects(self):
        return self._sender(endpoint="/projects/", method="GET")

    def get_sentry_project(self, project_name: str):
        return self._sender(endpoint=f'/projects/sentry/{project_name}/', method="GET")

    def get_sentry_project_keys(self, project_name: str):
        return self._sender(endpoint=f"/projects/sentry/{project_name}/keys/", method="GET")

    def create_sentry_project(self, team_name: str, project_name: str):
        data = {"name": project_name}
        return self._sender(endpoint=f"/teams/sentry/{team_name}/projects/", method="POST", data=data)

    def get_sentry_team(self, team_name: str):
        return self._sender(endpoint=f"/teams/sentry/{team_name}/", method="GET")

    def create_sentry_team(self, team_name: str):
        data = {"name": team_name}
        return self._sender(endpoint=f"/organizations/sentry/teams/", method="POST", data=data)
