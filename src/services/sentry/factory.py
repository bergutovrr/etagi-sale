from typing import TypeVar, List, Optional

from services.sentry import SentryProjectDto
from services.sentry.service import AbstractSentryService, SentryService

InheritedSentryService = TypeVar('InheritedSentryService', bound=AbstractSentryService)


class SentryServiceFactory:
    @classmethod
    def create_sentry_service(cls, is_test: bool = False) -> InheritedSentryService:
        if is_test:
            service = MockedSentryService()
        else:
            service = SentryService()
        return service


class MockedSentryService(AbstractSentryService):
    def get_sentry_project(self, project_name: str) -> Optional[SentryProjectDto]:
        pass

    def get_sentry_projects(self) -> List[SentryProjectDto]:
        pass

    def create_sentry_project(self, project_name: str, team_name: int) -> SentryProjectDto:
        pass
