from unittest.mock import Mock

import pytest

from conf import settings
from services.sentry.exceptions import SentryClientError
from services.sentry.sentryclient import AbstractSentryClient
from services.sentry.service import SentryService


class TestSentryService:
    sentry_client: AbstractSentryClient
    sentry_service: SentryService

    def _init_sentry_service(self):
        settings.SENTRY_URL_FOR_EXAMPLE = 'http://sentry-host'

        self.sentry_client = Mock(spec=AbstractSentryClient)
        self.sentry_client.get_sentry_project.return_value = TestSentryService._get_sentry_project()
        self.sentry_client.get_sentry_projects.return_value = TestSentryService._get_sentry_projects()
        self.sentry_client.get_sentry_project_keys.return_value = TestSentryService._get_sentry_project_keys()
        self.sentry_client.create_sentry_project.return_value = TestSentryService._get_sentry_project()
        self.sentry_client.get_sentry_team.return_value = TestSentryService._get_sentry_team()

        self.sentry_service = SentryService()
        self.sentry_service.sentry_client = self.sentry_client

    @classmethod
    def _get_sentry_projects(cls):
        return [TestSentryService._get_sentry_project(1), TestSentryService._get_sentry_project(2)]

    @classmethod
    def _get_sentry_project(cls, idx: int = 0):
        return {
            'id': 123 + idx,
            'name': f'test_project{idx}',
            'slug': f'test_project{idx}',
            'organization': {'slug': 'sentry'},
            'status': 'active',
            'DSN': f'sentry_dsn{idx}'
        }

    @classmethod
    def _get_sentry_project_keys(cls):
        return [{'dsn': {"secret": "sentry_dsn0"}}]

    @classmethod
    def _get_sentry_team(cls):
        return {"id": "2", "name": "test_project0", "slug": "test_project0"}

    # region get_sentry_project

    def test_get_sentry_project(self):
        self._init_sentry_service()

        project = self.sentry_service.get_sentry_project("test_project_name")
        assert project is not None
        assert project.project_id == 123
        assert project.name == 'test_project0'
        assert project.slug == 'test_project0'
        assert project.organization == 'sentry'
        assert project.status == 'active'
        assert project.dsn == 'sentry_dsn0'
        assert project.url == 'http://sentry-host/sentry/test_project0/'

    def test_get_sentry_project_check_client_params(self):
        self._init_sentry_service()

        self.sentry_service.get_sentry_project("test_project0")
        self.sentry_client.get_sentry_project.assert_called_once_with(project_name='test_project0')

    def test_get_sentry_project_error_without_project_name(self):
        self._init_sentry_service()

        with pytest.raises(TypeError):
            self.sentry_service.get_sentry_project(project_name=None)

    def test_get_sentry_project_client_error(self):
        self._init_sentry_service()

        self.sentry_client.get_sentry_project.side_effect = SentryClientError()
        with pytest.raises(SentryClientError):
            self.sentry_service.get_sentry_project(project_name="test_project0")

    def test_get_sentry_project_none(self):
        self._init_sentry_service()

        self.sentry_client.get_sentry_project.return_value = None
        project = self.sentry_service.get_sentry_project("test_project0")
        assert project is None

    def test_get_sentry_project_when_keys_not_found_dsn_is_none(self):
        self._init_sentry_service()

        self.sentry_client.get_sentry_project_keys.return_value = None
        project = self.sentry_service.get_sentry_project("test_project0")
        assert project is not None
        assert project.dsn is None

    # endregion

    # region get_sentry_projects

    def test_get_sentry_projects(self):
        self._init_sentry_service()

        sentry_projects = self.sentry_service.get_sentry_projects()
        assert sentry_projects is not None
        assert len(sentry_projects) == 2

    def test_get_sentry_projects_client_error(self):
        self._init_sentry_service()

        self.sentry_client.get_sentry_projects.side_effect = SentryClientError()
        with pytest.raises(SentryClientError):
            self.sentry_service.get_sentry_projects()

    # endregion

    # region create_sentry_project

    def test_create_sentry_project_with_existed_team(self):
        self._init_sentry_service()

        self.sentry_service.create_sentry_project("test_project0")
        self.sentry_client.get_sentry_team.assert_called_once_with(team_name='test_project0')
        self.sentry_client.create_sentry_project.assert_called_once_with(team_name='test_project0',
                                                                         project_name='test_project0')
        self.sentry_client.get_sentry_project_keys.assert_called_once_with(project_name='test_project0')

    def test_create_sentry_project_with_new_team(self):
        self._init_sentry_service()
        self.sentry_client.get_sentry_team.return_value = None

        self.sentry_service.create_sentry_project("test_project0")
        self.sentry_client.get_sentry_team.assert_called_once_with(team_name='test_project0')
        self.sentry_client.create_sentry_team.assert_called_once_with(team_name='test_project0')
        self.sentry_client.create_sentry_project.assert_called_once_with(team_name='test_project0',
                                                                         project_name='test_project0')
        self.sentry_client.get_sentry_project_keys.assert_called_once_with(project_name='test_project0')

    # endregion
