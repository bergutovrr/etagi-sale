from typing import List

from services.sentry.dto.project import SentryProjectDto
from services.sentry.service import AbstractSentryService


class SentryMocker:

    @staticmethod
    def mock_get_sentry_projects(mocker) -> List[SentryProjectDto]:
        sentry_projects = SentryProjectDto(
            project_id=1,
            name='test_project',
            slug='test_project',
            organization='sentry',
            status='active',
            url='http://sentry.itlabs.io/projects/sentry/test_project',
            dsn='dsn-here'
        )
        return mocker.patch('services.sentry.service.SentryService.', return_value=sentry_projects)


class MockedSentryService(AbstractSentryService):
    def create_sentry_project(self, name: str, group_id: int) -> SentryProjectDto:
        pass

    def get_sentry_projects(self) -> List[SentryProjectDto]:
        pass
