from dataclasses import dataclass
from typing import List

from conf import settings
from atlas_utils.common.dto import ItemDto
from atlas_utils.common.url import join


@dataclass
class SentryProjectDto(ItemDto):
    project_id: int
    name: str
    slug: str
    organization: str
    status: str
    url: str
    dsn: str = None


class SentryProjectDtoService:

    @staticmethod
    def get_list_dtos(objects: dict) -> List[SentryProjectDto]:
        list_dtos = []
        for object in objects:
            list_dtos.append(SentryProjectDtoService.object_to_dto(object=object, dsn=""))
        return list_dtos

    @staticmethod
    def object_to_dto(object: dict, dsn: str = None) -> SentryProjectDto:
        if dsn:
            secret = dsn
        else:
            secret = None
        project_name = object.get('name')
        return SentryProjectDto(project_id=object.get('id'),
                                name=project_name,
                                slug=object.get('slug'),
                                organization=object.get('organization', {}).get('slug'),
                                status=object.get('status'),
                                url=SentryProjectDtoService.get_sentry_project_url(project_name),
                                dsn=secret)

    @staticmethod
    def get_sentry_project_url(project_name: str) -> str:
        return join(settings.SENTRY_URL_FOR_EXAMPLE, f'/sentry/{project_name}/')
