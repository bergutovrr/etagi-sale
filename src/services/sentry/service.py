from abc import ABCMeta, abstractmethod
from typing import List, Optional

from services.sentry.dto.project import SentryProjectDtoService, SentryProjectDto
from services.sentry.sentryclient import SentryClient


class AbstractSentryService:
    __metaclass__ = ABCMeta

    @abstractmethod
    def get_sentry_project(self, project_name: str) -> Optional[SentryProjectDto]:
        raise NotImplementedError

    @abstractmethod
    def get_sentry_projects(self) -> List[SentryProjectDto]:
        raise NotImplementedError

    @abstractmethod
    def create_sentry_project(self, project_name: str, team_name: int) -> SentryProjectDto:
        raise NotImplementedError


class SentryService(AbstractSentryService):

    def __init__(self):
        self.sentry_client = SentryClient()

    def get_sentry_project(self, project_name: str) -> Optional[SentryProjectDto]:
        """
        Получение sentry-проекта
        """
        if project_name is None:
            raise TypeError('Не задано название проекта')

        project = self.sentry_client.get_sentry_project(project_name=project_name)
        if project is None:
            return None

        project_keys = self.sentry_client.get_sentry_project_keys(project_name)
        dsn = project_keys[0]['dsn']['secret'] if project_keys is not None else None

        return SentryProjectDtoService.object_to_dto(object=project, dsn=dsn)

    def get_sentry_projects(self) -> List[SentryProjectDto]:
        """
        Получение списка проектов в Sentry
        """
        projects = self.sentry_client.get_sentry_projects()
        return SentryProjectDtoService.get_list_dtos(projects)

    def create_sentry_project(self, project_name: str, team_name: str = None) -> SentryProjectDto:
        """
        Создание проекта в Sentry.
        Проект создается в организации 'sentry'.
        """
        if project_name is None:
            raise TypeError('Не задано название проекта')

        if team_name is None:
            team_name = project_name

        team = self.sentry_client.get_sentry_team(team_name=team_name)
        if team is None:
            self.sentry_client.create_sentry_team(team_name=team_name)

        project = self.sentry_client.create_sentry_project(team_name=team_name, project_name=project_name)
        project_keys = self.sentry_client.get_sentry_project_keys(project_name=project['slug'])

        dsn = project_keys[0]['dsn']['secret'] if project_keys is not None else None
        return SentryProjectDtoService.object_to_dto(object=project, dsn=dsn)
