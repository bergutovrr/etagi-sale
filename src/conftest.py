import pytest
from _pytest.mark import MarkInfo
from django.conf import settings
from envparse import env


@pytest.fixture(scope='session')
def django_db_modify_db_settings():
    settings.DATABASES['default']['NAME'] = env('POSTGRES_DB_NAME',  default='')


def pytest_collection_modifyitems(items):
    for item in items:
        all_markers = item.keywords._markers
        markers = ['api' in marker for marker in all_markers if type(all_markers[marker]) is MarkInfo]
        if markers.count(True) > 0:
            item.add_marker(pytest.mark.api)
