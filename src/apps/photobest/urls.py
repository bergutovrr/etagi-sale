from django.urls import path
from django.views.decorators import csrf

from apps.photobest.views import PhotoView, PhotoFileView

urlpatterns = [
    path('api/photo_process', csrf.csrf_exempt(PhotoView.as_view()), name='photo_view'),
    path('api/photo/<str:photo_name>', csrf.csrf_exempt(PhotoFileView.as_view()), name='photo_file_view')
]
