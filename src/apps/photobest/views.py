import uuid

from django.conf import settings
from django.http import JsonResponse, HttpResponse
from django.urls import reverse
from django.views import View

from apps.blocker.blocker import person_blocker
from apps.photobest.make_photo_better import make_good
from apps.redis_storage.repository import RedisRepository
from apps.yandex_disk.client import YandexDiskClient


class PhotoView(View):
    def post(self, request):
        photo = request.FILES.get('photo')
        photo = make_good(photo)
        check_photo = person_blocker(photo)#request.FILES.get('photo'))
        file_name = str(uuid.uuid4())
        RedisRepository.set_cache(file_name=file_name, file=check_photo)
        YandexDiskClient.load_file_and_get_link(file=check_photo, file_name=file_name)
        photo_path = reverse('photo_file_view', kwargs={'photo_name': file_name})
        link = f'{settings.SELF_URL}{photo_path}'
        return JsonResponse(data={'link': link})


class PhotoFileView(View):
    def get(self, request, photo_name):
        photo = RedisRepository.get_cache(file_name=photo_name)
        if not photo:
            photo = YandexDiskClient.get_file_by_name(file_name=photo_name)
            RedisRepository.set_cache(file_name=photo_name, file=photo)
        return HttpResponse(photo, content_type="image/jpeg")
