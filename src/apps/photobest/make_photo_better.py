from _warnings import warn

import cv2
import numpy
from skimage import exposure, img_as_ubyte
from skimage.io import imread, use_plugin


def load(f, as_gray=False, as_grey=None):
    if as_grey is not None:
        as_gray = as_grey
        warn('`as_grey` has been deprecated in favor of `as_gray`'
             ' and will be removed in v0.16.')

    use_plugin('pil')
    return imread(f, as_gray=as_gray)


def normalize_contrast(img):
    return exposure.equalize_adapthist(img)


def remove_noise(img):
    img = img_as_ubyte(img)
    return cv2.fastNlMeansDenoisingColored(img, None)  # , 10, 10, 7, 21)


def make_good(photo):
    img = cv2.imdecode(numpy.fromstring(photo.read(), numpy.uint8), cv2.IMREAD_UNCHANGED)
    img = normalize_contrast(img)
    img = remove_noise(img)
    retval, img = cv2.imencode('.jpg', img)
    return img.tostring()
