from django.apps import AppConfig


class PhotobestConfig(AppConfig):
    name = 'photobest'
