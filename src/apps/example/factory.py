from apps.example.dto import ExampleServiceDto, ExampleNewServiceDto
from apps.example.forms import CreateExampleServiceForm
from apps.example.models import Example
from atlas_utils.common.dto import ItemDtoFactory


class ExampleDtoFactory(ItemDtoFactory):
    @classmethod
    def dto_from_dict(cls, item_dict: dict) -> ExampleServiceDto:
        raise NotImplementedError

    @classmethod
    def dto_from_django_model(cls, item_model: Example) -> ExampleServiceDto:
        return ExampleServiceDto(
            name=item_model.name,
            short_name=item_model.short_name,
            example_id=item_model.id
        )


class ExampleNewServiceDtoFactory:
    @classmethod
    def dto_from_body(cls, body: dict) -> ExampleNewServiceDto:
        example_service_dto = ExampleNewServiceDto()
        if 'name' in body:
            example_service_dto.name = body.get('name')
        return example_service_dto

    @classmethod
    def dto_from_create_form(cls, form: CreateExampleServiceForm) -> ExampleNewServiceDto:
        data = form.cleaned_data
        return ExampleNewServiceDto(
            name=data.get('componentId'),
        )
