from django.urls import path
from django.views.decorators import csrf

from apps.example.views import ExampleView

urlpatterns = [
    path('api/<str:api_version>/hello', csrf.csrf_exempt(ExampleView.as_view()),
         name='example_view'),

]
