from apps.example.dto import ExampleServiceDto


class ExampleServicePresenter:
    @classmethod
    def dto_to_created_data(cls, dto: ExampleServiceDto):
        return {
            "example_id": dto.example_id,
            "name": dto.name,
            "short_name": dto.short_name,
        }

    @classmethod
    def dto_to_search_data(cls, dto: ExampleServiceDto):
        return {
            "id": dto.example_id,
            "name": dto.name,
        }
