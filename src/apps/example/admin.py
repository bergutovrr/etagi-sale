from django.contrib import admin

from apps.example.models import Example
from atlas_utils.common.actual_model import ActualModelAdmin


class ExampleAdmin(ActualModelAdmin):
    class Meta:
        model = Example

    list_display = ['name', 'short_name']


admin.site.register(Example, ExampleAdmin)
