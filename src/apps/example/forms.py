from django import forms


class CreateExampleServiceForm(forms.Form):
    name = forms.CharField(required=True)
