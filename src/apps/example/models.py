from django.db import models

from apps.example.specifications import EXAMPLE_CHOICES
from atlas_utils.common.actual_model import ActualModel


class Example(ActualModel):
    name = models.CharField(max_length=255)
    short_name = models.CharField(max_length=255)

    class Meta:
        verbose_name = 'Пример'
        verbose_name_plural = 'Примеры'

    def __str__(self):
        return self.name


class Parameter(ActualModel):
    page = models.CharField(max_length=50, choices=EXAMPLE_CHOICES)
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name
