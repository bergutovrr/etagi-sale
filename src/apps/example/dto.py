from dataclasses import dataclass

from atlas_utils.common.dto import ItemDto


@dataclass
class ExampleServiceDto(ItemDto):
    short_name: str
    name: str
    example_id: int


@dataclass
class ExampleNewServiceDto:
    name: int = None
