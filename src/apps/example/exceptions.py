from atlas_utils.common.exceptions import BaseError


class ExampleServiceCustomException(BaseError):
    status_code = 400

    def __init__(self):
        message = f'message'
        super().__init__(message=message)
