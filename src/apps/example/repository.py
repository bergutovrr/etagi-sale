from typing import List

from apps.example.dto import ExampleServiceDto, ExampleNewServiceDto
from apps.example.factory import ExampleDtoFactory
from apps.example.models import Example


class ExampleServiceRepository:

    @classmethod
    def get_service_by_id(cls, id: int) -> ExampleServiceDto:
        example_object = Example.objects.get(pk=id)
        return ExampleDtoFactory.dto_from_django_model(item_model=example_object)

    @classmethod
    def update_service(cls, dto_object: ExampleNewServiceDto, pk: int) -> ExampleServiceDto:
        example = Example.objects.get(pk=pk)
        """
        тут обновляем модель
        """
        return ExampleDtoFactory.dto_from_django_model(item_model=example)

    @classmethod
    def get_service_list_with_gitlab_group_id(cls, gitlab_group_id: int) -> List[ExampleServiceDto]:
        example_list = Example.objects.filter(gitlab_group_id=gitlab_group_id)
        return [ExampleDtoFactory.dto_from_django_model(item_model=exm) for exm in example_list]

    @classmethod
    def get_list(cls, ) -> List[ExampleServiceDto]:
        example_list = Example.objects.all()
        return [ExampleDtoFactory.dto_from_django_model(item_model=exm) for exm in example_list]
