from django.apps import AppConfig


class AtlasConfig(AppConfig):
    name = 'apps.example'
    verbose_name = 'Сервис Example'
