from django.conf import settings

EXAMPLE_CONSTANT = 'example'
EXAMPLE_1 = "exm1"
EXAMPLE_2 = "exm2"
EXAMPLE_3 = "exm3"

EXAMPLE_CHOICES = (
    (EXAMPLE_1, 'Example1'),
    (EXAMPLE_2, 'Example2'),
    (EXAMPLE_3, 'Example3'),
)


def get_example_url(ms_name: str, gitlab_branch: str) -> str:
    return f'{settings.API_VERSION}/{EXAMPLE_CONSTANT}'
