from typing import List

from apps.example.dto import ExampleServiceDto
from apps.example.repository import ExampleServiceRepository


class ExampleServiceService:
    """
    этот код приведен в виде примера
    """

    @classmethod
    def get_service_by_id(cls, service_id: int) -> ExampleServiceDto:
        return ExampleServiceRepository.get_service_by_id(service_id=service_id)

    @classmethod
    def update_service(self, service_id: object, update_object_dto: object) -> object:
        pass
        # return ExampleServiceRepository.update_service(service_id=service_id, confluence_service_dto=confluence_service_dto,
        #                                         gitlab_group=gitlab_group)

    @classmethod
    def get_service_list(cls) -> ExampleServiceDto:
        return ExampleServiceRepository.get_list()

    @classmethod
    def get_service_by_name(cls, name: str) -> List[ExampleServiceDto]:
        return ExampleServiceRepository.get_service_by_name(name=name)
