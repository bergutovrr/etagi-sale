from django import views
from django.utils.decorators import method_decorator

from apps.example.service import ExampleServiceService
from atlas_utils.common.exceptions import WrongApiVersionException
from atlas_utils.common.http_tools import api_call


class ExampleView(views.View):
    @method_decorator(api_call(method='ExampleService->POST'), name='dispatch')
    def post(self, request, api_version):
        if api_version == '0':
            data = {
                "entity_id": 1,
                "name": "тестовый сервис",
            }
        elif api_version == '1':
            data = ExampleServiceService.update_service()

        else:
            raise WrongApiVersionException(message='Неверная версия АПИ, доступные версии: 0 и 1')
        return {"items": data}

    @method_decorator(api_call(method='ExampleService->GET'), name='dispatch')
    def get(self, request, api_version):
        if api_version == '0':
            data = [
                {
                    "entity_id": 1,
                    "name": "тестовый сервис",
                },
                {
                    "entity_id": 2,
                    "name": "тестовый сервис",
                }
            ]

        elif api_version == '1':

            data = ExampleServiceService.get_service_list()
        else:
            raise WrongApiVersionException(message='Неверная версия АПИ, доступные версии: 0 и 1')
        return {"items": data}
