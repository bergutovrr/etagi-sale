import cv2
import imageio
import numpy as np

image = cv2.imread('person_blocked.png')
lower = np.array([0, 0, 0])
upper = np.array([15, 15, 15])

shapeMask = cv2.inRange(image, lower, upper)
cv2.imshow("Mask", shapeMask)
cv2.waitKey(0)
