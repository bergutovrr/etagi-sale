import uuid

import requests
import ujson
from django.conf import settings


class YandexDiskClient:
    token = settings.YANDEX_TOKEN

    @classmethod
    def get_path(cls, image_path):
        url = 'https://cloud-api.yandex.net/v1/disk/resources/upload'
        headers = {
            'Authorization': cls.token
        }
        params = {
            'path': image_path
        }
        resp = requests.get(url, params=params, headers=headers)
        data = ujson.loads(resp.text)
        href = data.get('href')
        return href

    @classmethod
    def load(cls, path, photo):
        requests.put(path, files={'file': photo})

    @classmethod
    def get_link_for_file(cls, path):
        url = 'https://cloud-api.yandex.net/v1/disk/resources/download'
        params = {
            'path': path
        }
        headers = {
            'Authorization': cls.token
        }
        resp = requests.get(url, params=params, headers=headers)
        data = ujson.loads(resp.text)
        href = data.get('href')
        return href

    @classmethod
    def load_file_and_get_link(cls, file_name, file):
        path = cls.get_path(file_name)
        cls.load(path=path, photo=file)

    @classmethod
    def download_file(cls, path):
        resp = requests.get(path)
        return resp.content

    @classmethod
    def get_file_by_name(cls, file_name: str):
        path = cls.get_link_for_file(file_name)
        return cls.download_file(path=path)
