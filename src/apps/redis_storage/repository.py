from django.core.cache import cache


class RedisRepository:
    @classmethod
    def get_cache(cls, file_name: str):
        cache_key = f'photo_{file_name}'
        return cache.get(cache_key)

    @classmethod
    def set_cache(cls, file_name: str, file):
        cache_key = f'photo_{file_name}'
        cache.set(cache_key, file)
