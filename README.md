Проект-болванка для работы на django + postgresql

Инструкция для разработчика по разворачиванию проекта:

```
git clone https://gl.sdvor.com/e2/boilerplate/django_template.git
python3.7 -m venv env
source env/bin/activate
pip install -r requirements.txt
# подтянуть переменные виртуального окружения в .env файл
# пример находится в env.example
./manage.py runserver
```

env.example - переменные конфигурации:
```
#режим для разработки см. документацию django
DEBUG=True

#Database configuration

POSTGRES_DB_NAME=db_name
POSTGRES_DB_USER=username
POSTGRES_DB_PASSWORD=password
POSTGRES_DB_HOST=db_address
POSTGRES_DB_PORT=5432

#Lobster configuration - авторизация

LOBSTER_TOKEN_SECRET='secret'
```

Подолнительные требования для работы проекта:
* база данных - postgresql
 

Для применения миграций выполняем команду:
```
./manage.py migrate
```
В миграциях предусмотрено создание пользователя для админки
логин: admin пароль: password
