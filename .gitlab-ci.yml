stages:
  - build
  - tests
  - tag-latest
  - analysis
  - deploy
  - publish_docs
  - cleanup
  - merge
  - analysis-dev

variables:
  GIT_SSL_NO_VERIFY: "true"
  SERVICE_NAME: etagi-sale

build:
  stage: build
  retry: 1
  script:
    - docker build -t $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG -f devops/Dockerfile .
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG

.run-tests:
  stage: tests
  image: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  tags: ['docker']
  services:
    - postgres:9.6
  variables:
    POSTGRES_USER: test_user
    POSTGRES_PASSWORD: test_password
    POSTGRES_DB_NAME: db
    POSTGRES_DB_USER: test_user
    POSTGRES_DB_PASSWORD: test_password
    POSTGRES_DB_HOST: postgres
    POSTGRES_DB_PORT: "5432"
    DEBUG: "True"
  script:
    - coverage run -m pytest src
    - coverage xml -o coverage-reports/coverage.xml
    - coverage report -m
  artifacts:
    paths:
    - $CI_PROJECT_DIR/coverage-reports
    expire_in: 30 days

run-pylint:
  stage: tests
  image: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  tags: ['sonarqube']
  script:
    - cd ./src/ && pylint --jobs=0 --rcfile=.pylintrc --exit-zero -r n --msg-template="{path}:{line}:\ [{msg_id}({symbol}), {obj}] {msg}" * > $CI_PROJECT_DIR/pylint.report
  artifacts:
    expire_in: 30 days
    paths:
      - $CI_PROJECT_DIR/pylint.report

tag-latest:
  stage: tag-latest
  script:
    - docker tag $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG $CI_REGISTRY_IMAGE
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker push $CI_REGISTRY_IMAGE
  only:
    - master

.sonarqube-analyze:
  stage: analysis
  image: registry.gl.sdvor.com/devops/sonarqube-scanner
  tags: ["sonarqube"]
  retry: 1
  variables:
    SONAR_URL: https://sonarqube.itlabs.io
    SONAR_ANALYSIS_MODE: publish
    SONAR_PROJECT_KEY: ${SERVICE_NAME}_${CI_COMMIT_REF_SLUG}_${CI_PROJECT_ID}
  cache:
    key: "sonar"
    paths:
      - ${CI_PROJECT_DIR}/.sonar
  before_script:
    - mkdir -p ${CI_PROJECT_DIR}/.sonar
    - ln -s ${CI_PROJECT_DIR}/.sonar ~/
  script:
    - gitlab-sonar-scanner
        -Dsonar.gitlab.all_issues=true
        -Dsonar.python.pylint_config=${CI_PROJECT_DIR}/src/.pylintrc
        -Dsonar.python.pylint.reportPath=${CI_PROJECT_DIR}/pylint.report
        -Dsonar.python.coverage.reportPaths=${CI_PROJECT_DIR}/coverage-reports/cov*.xml
  only:
    - release
    - master


deploy-to-development:
  stage: deploy
  image: registry.gl.sdvor.com/quality/atlas-checker
  tags: ['docker']
  environment:
    name: development
  variables:
    KUBE_NAMESPACE: government-dev
    MANIFEST_FOLDER: ./devops/kubernetes/
    ENVIRONMENT: dev
  script:
    - atlas-cli deploy
  only:
    - development


deploy-to-stage:
  stage: deploy
  image: registry.gl.sdvor.com/quality/atlas-checker
  tags: ['docker']
  environment:
    name: stage
  variables:
    KUBE_NAMESPACE: default
    MANIFEST_FOLDER: ./devops/kubernetes/
    ENVIRONMENT: stage
  script:
    - atlas-cli deploy
  only:
    - release


deploy-to-production:
  stage: deploy
  image: registry.gl.sdvor.com/quality/atlas-checker
  tags: ['docker']
  environment:
    name: production
  variables:
    KUBE_NAMESPACE: default
    MANIFEST_FOLDER: ./devops/kubernetes/
    ENVIRONMENT: prod
  script:
    - atlas-cli deploy
  only:
    - master



publish-swagger-doc:
  stage: publish_docs
  environment:
    name: swagger/$CI_COMMIT_REF_SLUG
    url: http://swagger.itlabs.io/
  script:
    - ansible ciara -m copy -a "dest=/srv/projects/swagger.itlabs.io/specs/etagi-sale_$CI_COMMIT_REF_SLUG src=$CI_PROJECT_DIR/swagger.yaml"
  only:
    - development
    - release
    - master

cleanup:
  stage: cleanup
  when: always
  allow_failure: true
  variables:
    GIT_STRATEGY: none
  script:
  - docker rmi -f $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG || true

release-to-dev:
  stage: merge
  script:
    - automerge.sh release development
  only:
    - release

master-to-release:
  stage: merge
  script:
    - automerge.sh master release
  only:
    - master

sonarqube-analyze:
  stage: analysis
  image: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  tags: ["sonarqube"]
  retry: 1
  script:
    - echo "hello"
